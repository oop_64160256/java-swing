package com.nipitpon.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class BottonExample1 extends JFrame {
    JButton button;
    JButton clearButton;
    JTextField text;
    public BottonExample1() {
        super("BottonExample");
        text = new JTextField();
        text.setBounds(50, 50, 150, 20);
        button = new JButton("Click Here");
        button.setBounds(50, 100, 95, 30);
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Click Botton");
                text.setText("Welcome to Burapha");
            }
        });
        clearButton = new JButton("Clear");
        clearButton.setBounds(165, 100, 95, 30);
        clearButton.setIcon(new ImageIcon("eraser.png"));
        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
                
            }

        });
        this.add(clearButton);
        this.add(button);
        this.add(text);
        this.setSize(400, 400);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        BottonExample1 frame = new BottonExample1();
    }
}
