package com.nipitpon.swingtutorial;

import javax.swing.*;

class MyApp {
    JFrame frame;
    JButton botton;

    MyApp() {
        frame = new JFrame();
        botton = new JButton();
        frame.setTitle("First JFrame");
        botton.setText("Click");
        botton.setBounds(220, 50, 70, 35); // set position and size of this botton
        frame.setLayout(null); // if you dont do this your botton's size will be same size with the frame
        frame.add(botton); // add botton to frame
        frame.setSize(350, 300); // set your frame size
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // you must do this for make sure your frame close
                                                              // completely
        frame.setVisible(true); // show your frame
    }
}

public class Simple {
    public static void main(String[] args) {
        MyApp app = new MyApp();
    }
}
