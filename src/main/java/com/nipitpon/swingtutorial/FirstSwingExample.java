package com.nipitpon.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("First JFrame");  //create frame and set the Title of a frame
        JButton botton = new JButton("Click");      //create botton and set text in this botton
        botton.setBounds(220, 50, 70, 35);          //set position and size of this botton 
        frame.setLayout(null);                      //if you dont do this your botton's size will be same size with the frame
        frame.add(botton);                          //add botton to frame
        frame.setSize(350, 300);                    //set your frame size 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   //you must do this for make sure your frame close completely
        frame.setVisible(true);                     //show your frame 
    }
}
