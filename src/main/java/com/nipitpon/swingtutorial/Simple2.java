package com.nipitpon.swingtutorial;

import javax.swing.*;

class MyFrame extends JFrame{
    JButton botton;

    MyFrame() {
        super("First JFrame");
        botton = new JButton();
        botton.setText("Click");
        botton.setBounds(220, 50, 70, 35); // set position and size of this botton
        this.setLayout(null); // if you dont do this your botton's size will be same size with the frame
        this.add(botton); // add botton to frame
        this.setSize(350, 300); // set your frame size
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // you must do this for make sure your frame close
                                                              // completely
        this.setVisible(true); // show your frame
    }
}

public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
    }
}
